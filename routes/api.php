<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\v1\MasterItemController;
use App\Http\Controllers\Api\v1\StockItemController;
use App\Http\Controllers\Api\v1\TransaksiController;
use Illuminate\Support\Facades\Route;

Route::post('v1/login', 'api\v1\AuthController@login');
Route::post('v1/register', 'api\v1\AuthController@register');

Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1', 'middleware' => 'apiJwt'], function () {
    Route::post('logout', 'AuthController@logout');

    Route::apiResources([
        '/master_item' => MasterItemController::class,
        '/stock_item' => StockItemController::class,
        '/transaksi' => TransaksiController::class,
    ]);

    Route::post('master_item/store', 'MasterItemController@store');
    Route::post('stock_item/store', 'StockItemController@store');
    Route::post('transaksi/store','TransaksiController@store');
});
