<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Master Item table
        Schema::create('master_item', function (Blueprint $table) {
            $table->id();
            $table->string('Company');
            $table->string('ItemType')->nullable();
            $table->string('Code');
            $table->string('ItemGroup')->nullable();
            $table->string('ItemAccountGroup')->nullable();
            $table->string('ItemUnit')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->timestamps();
        });
        // Transaction table
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->string('Company');
            $table->string('CompanyName');
            $table->string('Code');
            $table->string('Date')->nullable();
            $table->string('Account')->nullable();
            $table->string('AccountName')->nullable();
            $table->string('Note')->nullable();
            $table->timestamps();
        });
        // Stock Item table
        Schema::create('stock_item', function (Blueprint $table) {
            $table->id();
            $table->string('index')->nullable();
            $table->string('Item')->nullable();
            $table->string('ItemName')->nullable();
            $table->integer('Quantity')->nullable();
            $table->string('ItemUnit')->nullable();
            $table->string('ItemUnitName')->nullable();
            $table->string('Note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
