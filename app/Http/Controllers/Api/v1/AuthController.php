<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('apiJwt', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = User::where('email',$request->email)->first();
        return response()->json([
            'user' => [
                'UserName' => $user->UserName,
                'Company' => $user->Company,
            ],
            'browserInfo' => [
                'chrome' => true,
                'chrome_view' => false,
                'chrome_mobile' => false,
                'chrome_mobile_ios' => false,
                'safari' => false,
                'safari_mobile' => false,
                'msedge' => false,
                'msie' => false
            ],
            'machineInfo' => [
                'brand' => "Apple",
                'model' => "",
                'os_name' => "mac",
                'type' => "desktop"
            ],
            'osNameInfo' => [
                'name' => "Mac",
                'version' => "10.15",
                'platform' => ""
            ],
            "Device" => $user->Device,
            "Model" => "Admin Web",
            "Source" => "103.242.150.163",
            "Exp" => "3",
            $this->respondWithToken($token)
        ]);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'UserName' => 'required|string',
            'Company' => 'required|string',
            'password' => 'required|string',
            'Device' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => Hash::make($request->password)]
        ));

        return response()->json([
            'message' => "User successfully registered",
            'user' => [
                'userName' => $user->UserName,
                'Company' => $user->Company,
            ],
            'browserInfo' => [
                'chrome' => true,
                'chrome_view' => false,
                'chrome_mobile' => false,
                'chrome_mobile_ios' => false,
                'safari' => false,
                'safari_mobile' => false,
                'msedge' => false,
                'msie' => false
            ],
            'machineInfo' => [
                'brand' => "Apple",
                'model' => "",
                'os_name' => "mac",
                'type' => "desktop"
            ],
            'osNameInfo' => [
                'name' => "Mac",
                'version' => "10.15",
                'platform' => ""
            ],
            "Device" => $user->device,
            "Model" => "Admin Web",
            "Source" => "103.242.150.163",
            "Exp" => "3"
        ], 201);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
