<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
class TransaksiController extends Controller
{
    public function __construct(){
        return $this->middleware('apiJwt');
    }

    public function index()
    {
        // Retrieve all transaksi
        $transaksi = Transaction::all();

        // Return the collection as JSON response
        return response()->json([
            'data' => $transaksi,
            'message' => 'Successfully retrieved items.',
        ], 200);
    }
    /**
    * ## Create Transaksi
    *
    * @OA\POST(
    *   tags={"/transaksi"},
    *   path="/transaksi/store",
    *   description="Create new Transaksi",
    *   security={{"bearerAuth": {}}},
    *   @OA\RequestBody(
    *       required=true,
    *       @OA\JsonContent(
    *           type="object",
    *           @OA\Property(property="Date", type="string", format="binary", example="2023-12-28"),
    *           @OA\Property(property="Account", type="string", format="binary", example="bc54db2f-4b44-4401-be7d-31c21effa9c1"),
    *           @OA\Property(property="AccountName", type="string", format="binary", example="Biaya Adm Bank - 800-01 - 800-01 (testcase)"),
    *           @OA\Property(property="Note", type="string", format="binary", example="null"),
    *       )
    *   ),
    *   @OA\Response(response=200, description="OK"),
    *   @OA\Response(response=400, description="Bad Request"),
    *   @OA\Response(response=403, description="Unauthorized"),
    * )
    *
    */
    function store(Request $request) {
        //validate data
        $validator = Validator::make($request->all(), [
            'Date' => 'string',
            'Account' => 'string',
            'AccountName' => 'string',
            'Note' => 'string',
        ]);
        $transaksi = Transaction::create(array_merge($validator->validated(),['Code' => md5(uniqid()),'Company' => Auth::user()->Company, 'CompanyName' => Auth::user()->UserName]));
        if ($transaksi) {
            return response()->json([
                'Company' => $transaksi->Company,
                'CompanyName' => $transaksi->CompanyName,
                'Code' => $transaksi->Code,
                'Date' => $transaksi->Date,
                'Account' => $transaksi->Account,
                'AccounttName' => $transaksi->AccounttName,
                'Note' => $transaksi->Note,
                'message'=>'Successfully created item.',
            ], 201);
            }else{
                return response()->json(['error'=>'Failed to create new item.'],400
                );
            }
    }
    //get all data
    /**
     * ## Show All Transaksi
*
* @OA\Get(
*   tags={"/transaksi"},
*   path="/transaksi/show",
*   description="Get all Transaksi",
*   security={{"bearerAuth": {}}},
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized")
* )
     */
    public function show(){
        $items=Transaction::all();
        return response()->json($items);
    }

    /**
     * ## Get Transaksi By ID
*
* @OA\Get(
*   tags={"/transaksi"},
*   path="/transaksi/{id}",
*   description="Get Transaksi By ID",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Transaksi ID",
*       in="path",
*       name="id",
*       example="1",
*       required=true,
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
     */
    public function find($id){
        $data = Transaction::where('Code',$id)->first();
        if(!$data) {
            return response()->json(['message' => 'Data Not Found'], 404);
            }else{
                return response()->json($data);
                }
            }
    /**
     * ## Update Transaksi
     * * @OA\Put(
     *      path="/transaksi/{id}",
     *      operationId="transaksi_update",
     *      tags={"/transaksi"},
     *      summary="Update Transaksi by ID",
     *      description="Updates an existing Transaksi by ID.",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="Transaksi ID",
     *          in="path",
     *          name="id",
     *          example="1",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="Date", type="string", format="binary", example="2023-12-28"),
    *           @OA\Property(property="Account", type="string", format="binary", example="bc54db2f-4b44-4401-be7d-31c21effa9c1"),
    *           @OA\Property(property="AccountName", type="string", format="binary", example="Biaya Adm Bank - 800-01 - 800-01 (testcase)"),
    *           @OA\Property(property="Note", type="string", format="binary", example="null"),
     *          )
     *      ),
     *      @OA\Response(response=200, description="OK"),
     *      @OA\Response(response=400, description="Bad Request"),
     *      @OA\Response(response=403, description="Unauthorized"),
     *      @OA\Response(response=404, description="Not Found")
     * )
     *
     */
    public static function update(Request $request,$id)
    {
        $Transaction = Transaction::find($id);

        if (!$Transaction) {
            return response()->json(['message' => 'Transaksi not found'], 404);
        }

        //validate data
        $validator = Validator::make($request->all(), [
            'Date' => 'string',
            'Account' => 'string',
            'AccountName' => 'string',
            'Note' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $Transaction->update($validator->validated());

        return response()->json($Transaction);
        }
        /**
         * * ## Delete Transaksi By Id ##
*
* @OA\DELETE(
*   tags={"/transaksi"},
*   path="/transaksi/{id}",
*   description="Delete Transaksi by Id",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Transaksi id",
*       in="path",
*       name="id",
*       example="3",
*       required=true,
*       @OA\Schema(
*           type="integer",
*           format="int64"
*       )
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
         */
        public function destroy($id)
        {
            // Check is user has permission to delete this data.
            $Transaction = Transaction::findOrFail($id);
            $Transaction->delete();
            return response()->json($Transaction->CompanyName."'s Transaction Deleted Successfully");
            }
        }
