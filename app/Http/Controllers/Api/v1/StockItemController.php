<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\StockItem;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;

class StockItemController extends Controller
{
    public function __construct(){
        return $this->middleware('apiJwt');
    }

    public function index()
    {
        // Retrieve all stockitems
        $stockitems = StockItem::all();

        // Return the collection as JSON response
        return response()->json([
            'data' => $stockitems,
            'message' => 'Successfully retrieved items.',
        ], 200);
    }
    /**
    * ## Create Stock Item
    *
    * @OA\POST(
    *   tags={"/stock_item"},
    *   path="/stock_item/store",
    *   description="Create new Stock Item",
    *   security={{"bearerAuth": {}}},
    *   @OA\RequestBody(
    *       required=true,
    *       @OA\JsonContent(
    *           type="object",
    *           @OA\Property(property="Index", type="string", format="binary", example="d3170153-6b16-4397-bf89-96533ee149ee"),
    *           @OA\Property(property="Item", type="string", format="binary", example="3adfb47a-eab4-4d44-bde9-efae1bec8543"),
    *           @OA\Property(property="ItemName", type="string", format="binary", example="Item A"),
    *           @OA\Property(property="Quantity", type="string", format="binary", example="1"),
    *           @OA\Property(property="ItemUnit", type="string", format="binary", example="4fc9683e-f22b-47c6-9525-b054ba24ea42"),
    *           @OA\Property(property="ItemUnitName", type="string", format="binary", example="PCS - PCS"),
    *           @OA\Property(property="Note", type="string", format="binary", example="null"),
    *       )
    *   ),
    *   @OA\Response(response=200, description="OK"),
    *   @OA\Response(response=400, description="Bad Request"),
    *   @OA\Response(response=403, description="Unauthorized"),
    * )
    *
    */
    function store(Request $request) {
        //validate data
        $validator = Validator::make($request->all(), [
            'Index' => 'string',
            'Item' => 'string',
            'ItemName' => 'string',
            'Quantity' => 'string',
            'ItemUnit' => 'string',
            'ItemUnitName' => 'string',
            'Note' => 'string'
        ]);
        $stock_item = StockItem::create($validator->validated());
        if ($stock_item) {
            return response()->json([
                'Index' => $stock_item->Index,
                'Item' => $stock_item->Item,
                'ItemName' => $stock_item->ItemName,
                'Quantity' => $stock_item->Quantity,
                'ItemUnit' => $stock_item->ItemUnit,
                'ItemUnitName' => $stock_item->ItemUnitName,
                'Note' => $stock_item->Note,
                'message'=>'Successfully created Stock item.',
            ], 201);
            }else{
                return response()->json(['error'=>'Failed to create new Stock item.'],400
                );
            }
    }
    //get all data
    /**
     * ## Show All Stock Item
*
* @OA\Get(
*   tags={"/stock_item"},
*   path="/stock_item/show",
*   description="Get all Stock Item",
*   security={{"bearerAuth": {}}},
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized")
* )
     */
    public function show(){
        $items=StockItem::all();
        return response()->json($items);
    }

    /**
     * ## Get Stock Item By ID
*
* @OA\Get(
*   tags={"/stock_item"},
*   path="/stock_item/{id}",
*   description="Get Stock Item By ID",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Stock Item ID",
*       in="path",
*       name="id",
*       example="1",
*       required=true,
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
     */
    public function find($id){
        $data = StockItem::where('Code',$id)->first();
        if(!$data) {
            return response()->json(['message' => 'Data Not Found'], 404);
            }else{
                return response()->json($data);
                }
            }
    /**
     * ## Update Stock Item
     * * @OA\Put(
     *      path="/stock_item/{id}",
     *      operationId="stock_item_update",
     *      tags={"/stock_item"},
     *      summary="Update Stock Item by ID",
     *      description="Updates an existing Stock Item by ID.",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="Stock Item ID",
     *          in="path",
     *          name="id",
     *          example="1",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="Index", type="string", format="binary", example="d3170153-6b16-4397-bf89-96533ee149ee"),
    *           @OA\Property(property="Item", type="string", format="binary", example="3adfb47a-eab4-4d44-bde9-efae1bec8543"),
    *           @OA\Property(property="ItemName", type="string", format="binary", example="Item A"),
    *           @OA\Property(property="Quantity", type="string", format="binary", example="1"),
    *           @OA\Property(property="ItemUnit", type="string", format="binary", example="4fc9683e-f22b-47c6-9525-b054ba24ea42"),
    *           @OA\Property(property="ItemUnitName", type="string", format="binary", example="PCS - PCS"),
    *           @OA\Property(property="Note", type="string", format="binary", example="null"),
     *          )
     *      ),
     *      @OA\Response(response=200, description="OK"),
     *      @OA\Response(response=400, description="Bad Request"),
     *      @OA\Response(response=403, description="Unauthorized"),
     *      @OA\Response(response=404, description="Not Found")
     * )
     *
     */
    public static function update(Request $request,$id)
    {
        $StockItem = StockItem::find($id);

        if (!$StockItem) {
            return response()->json(['message' => 'Stock Item not found'], 404);
        }

        //validate data
        $validator = Validator::make($request->all(), [
            'Index' => 'required|string',
            'Item' => 'string',
            'ItemName' => 'string',
            'Quantity' => 'string',
            'ItemUnit' => 'string',
            'ItemUnitName' => 'string',
            'Note' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $StockItem->update($validator->validated());

        return response()->json($StockItem);
        }
        /**
         * * ## Delete Stock Item By Id ##
*
* @OA\DELETE(
*   tags={"/stock_item"},
*   path="/stock_item/{id}",
*   description="Delete Stock Item by Id",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Stock Item id",
*       in="path",
*       name="id",
*       example="3",
*       required=true,
*       @OA\Schema(
*           type="integer",
*           format="int64"
*       )
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
         */
        public function destroy($id)
        {
            // Check is user has permission to delete this data.
            $StockItem = StockItem::findOrFail($id);
            $StockItem->delete();
            return response()->json($StockItem->ItemName." Deleted Successfully");
            }
}
