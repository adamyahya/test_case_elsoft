<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterItem;
use Validator;

class MasterItemController extends Controller
{
    public function __construct(){
        return $this->middleware('apiJwt');
    }

    public function index()
    {
        // Retrieve all MasterItems
        $masterItems = MasterItem::all();

        // Return the collection as JSON response
        return response()->json([
            'data' => $masterItems,
            'message' => 'Successfully retrieved items.',
        ], 200);
    }
    /**
    * ## Create Master Item
    *
    * @OA\POST(
    *   tags={"/master_item"},
    *   path="/master_item/store",
    *   description="Create new Master Item",
    *   security={{"bearerAuth": {}}},
    *   @OA\RequestBody(
    *       required=true,
    *       @OA\JsonContent(
    *           type="object",
    *           @OA\Property(property="Company", type="string", format="binary", example="d3170153-6b16-4397-bf89-96533ee149ee"),
    *           @OA\Property(property="ItemType", type="string", format="binary", example="3adfb47a-eab4-4d44-bde9-efae1bec8543"),
    *           @OA\Property(property="Label", type="string", format="binary", example="Item A"),
    *           @OA\Property(property="ItemGroup", type="string", format="binary", example="55692914-7402-4dd8-adec-40a823222b3e"),
    *           @OA\Property(property="ItemAccountGroup", type="string", format="binary", example="4fc9683e-f22b-47c6-9525-b054ba24ea42"),
    *           @OA\Property(property="ItemUnit", type="string", format="binary", example="5daf6a23-472d-4921-9945-57674d5fd1aa"),
    *           @OA\Property(property="IsActive", type="string", format="binary", example="true"),
    *       )
    *   ),
    *   @OA\Response(response=200, description="OK"),
    *   @OA\Response(response=400, description="Bad Request"),
    *   @OA\Response(response=403, description="Unauthorized"),
    * )
    *
    */
    function store(Request $request) {
        //validate data
        $validator = Validator::make($request->all(), [
            'Company' => 'required|string',
            'ItemType' => 'string',
            'Label' => 'string',
            'ItemGroup' => 'string',
            'ItemAccountGroup' => 'string',
            'ItemUnit' => 'string',
            'IsActive' => 'string'
        ]);
        $master_item = MasterItem::create(array_merge($validator->validated(),['Code' => md5(uniqid())]));
        if ($master_item) {
            return response()->json([
              'Company' => $master_item->Company,
              'ItemType' => $master_item->ItemType,
              'Code' => $master_item->Code,
              'Label' => $master_item->Label,
              'ItemGroup' => $master_item->ItemGroup,
              'ItemAccountGroup' => $master_item->ItemAccountGroup,
              'ItemUnit' => $master_item->ItemUnit,
              'IsActive' => $master_item->IsActive,
              'message'=>'Successfully created item.',
              ], 201);
            }else{
                return response()->json(['error'=>'Failed to create new item.'],400
                );
            }
    }
    //get all data
    /**
     * ## Show All Master Item
*
* @OA\Get(
*   tags={"/master_item"},
*   path="/master_item/show",
*   description="Get all Master Item",
*   security={{"bearerAuth": {}}},
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized")
* )
     */
    public function show(){
        $items=MasterItem::all();
        return response()->json($items);
    }

    /**
     * ## Get Master Item By ID
*
* @OA\Get(
*   tags={"/master_item"},
*   path="/master_item/{id}",
*   description="Get Master Item By ID",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Master Item ID",
*       in="path",
*       name="id",
*       example="1",
*       required=true,
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
     */
    public function find($id){
        $data = MasterItem::where('Code',$id)->first();
        if(!$data) {
            return response()->json(['message' => 'Data Not Found'], 404);
            }else{
                return response()->json($data);
                }
            }
    /**
     * ## Update Master Item
     * * @OA\Put(
     *      path="/master_item/{id}",
     *      operationId="master_item_update",
     *      tags={"/master_item"},
     *      summary="Update Master Item by ID",
     *      description="Updates an existing Master Item by ID.",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="Master Item ID",
     *          in="path",
     *          name="id",
     *          example="1",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="Company", type="string", format="binary", example="d3170153-6b16-4397-bf89-96533ee149ee"),
    *           @OA\Property(property="ItemType", type="string", format="binary", example="3adfb47a-eab4-4d44-bde9-efae1bec8543"),
    *           @OA\Property(property="Label", type="string", format="binary", example="Item A"),
    *           @OA\Property(property="ItemGroup", type="string", format="binary", example="55692914-7402-4dd8-adec-40a823222b3e"),
    *           @OA\Property(property="ItemAccountGroup", type="string", format="binary", example="4fc9683e-f22b-47c6-9525-b054ba24ea42"),
    *           @OA\Property(property="ItemUnit", type="string", format="binary", example="5daf6a23-472d-4921-9945-57674d5fd1aa"),
    *           @OA\Property(property="IsActive", type="string", format="binary", example="true"),
     *          )
     *      ),
     *      @OA\Response(response=200, description="OK"),
     *      @OA\Response(response=400, description="Bad Request"),
     *      @OA\Response(response=403, description="Unauthorized"),
     *      @OA\Response(response=404, description="Not Found")
     * )
     *
     */
    public static function update(Request $request,$id)
    {
        $masteritem = MasterItem::find($id);

        if (!$masteritem) {
            return response()->json(['message' => 'Master Item not found'], 404);
        }

        //validate data
        $validator = Validator::make($request->all(), [
            'Company' => 'required|string',
            'ItemType' => 'string',
            'Label' => 'string',
            'ItemGroup' => 'string',
            'ItemAccountGroup' => 'string',
            'ItemUnit' => 'string',
            'IsActive' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $masteritem->update($validator->validated());

        return response()->json($masteritem);
        }
        /**
         * * ## Delete Master Iten By Id ##
*
* @OA\DELETE(
*   tags={"/master_item"},
*   path="/master_item/{id}",
*   description="Delete master item by Id",
*   security={{"bearerAuth": {}}},
*   @OA\Parameter(
*       description="Master Item id",
*       in="path",
*       name="id",
*       example="3",
*       required=true,
*       @OA\Schema(
*           type="integer",
*           format="int64"
*       )
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
*   @OA\Response(response=403, description="Unauthorized"),
*   @OA\Response(response=404, description="Not Found")
* )
         */
        public function destroy($id)
        {
            // Check is user has permission to delete this data.
            $masteritem = MasterItem::findOrFail($id);
            $masteritem->delete();
            return response()->json($masteritem->Label." Deleted Successfully");
            }
       }
