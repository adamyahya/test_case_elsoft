<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
*
* @OA\Info(
*   version="1.0",
*   title="Test Case Elsoft Rest FUll API",
*   description="Using Newest API Documentation Generator With Swagger.. 
* Nama : Adam Yahya
* No HP : 082268528793
* Email : Adamyahyam@gmail.com
* Please Register First Before Login To Get Access Token, /user/register",
*   @OA\Contact(
*       name="Adam Yahya",
*       email="Adamyahyam@gmail.com",
*       url="https://www.linkedin.com/in/adamyy/"
*      )
* )
* @OA\Server(
*     url="http://localhost:8000/api/v1",
*     description="API server"
* )
* @OA\SecurityScheme(
*   type="http",
*   scheme="bearer",
*   securityScheme="bearerAuth",
* )
*
* ## Register User ##
*
* @OA\POST(
*   tags={"/users"},
*   path="/register",
*   description="Register new User",
*   security={{"bearerAuth": {}}},
*   @OA\RequestBody(
*       required=true,
*       @OA\JsonContent(
*           type="object",
*           @OA\Property(property="UserName", type="string", format="binary", example="testcase"),
*           @OA\Property(property="password", type="string", format="binary", example="testcase123"),
*           @OA\Property(property="email", type="string", format="binary", example="testcase@gmail.com"),
*           @OA\Property(property="Company", type="string", format="binary", example="b362-jh21-kl32-poe3"),
*           @OA\Property(property="Device", type="string", format="binary", example="Desktop"),
*       )
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
* )
*
* ## Login with User ##
*
* @OA\POST(
*   tags={"/users"},
*   path="/login",
*   description="Login wiht User",
*   security={{"bearerAuth": {}}},
*   @OA\RequestBody(
*       required=true,
*       @OA\JsonContent(
*           type="object",
*           @OA\Property(property="email", type="string", format="binary", example="testcase@gmail.com"),
*           @OA\Property(property="password", type="string", format="binary", example="testcase123"),
*       )
*   ),
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
* )
*
* ## Logout User ##
*
* @OA\POST(
*   tags={"/users"},
*   path="/logout",
*   description="Logout current user",
*   security={{"bearerAuth": {}}},
*   @OA\Response(response=200, description="OK"),
*   @OA\Response(response=400, description="Bad Request"),
* )
*

*/


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
