<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterItem extends Model
{
    use HasFactory;
    protected $table = 'master_item';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'Company',
            'ItemType',
            'Code',
            'Label',
            'ItemGroup',
            'ItemAccountGroup',
            'ItemUnit',
            'IsActive'
    ];
}
