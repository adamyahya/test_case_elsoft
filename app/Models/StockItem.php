<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockItem extends Model
{
    use HasFactory;
    protected $table = 'stock_item';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'Index',
            'Item',
            'ItemName',
            'Quantity',
            'ItemUnit',
            'ItemUnitName',
            'Note',
    ];
}
